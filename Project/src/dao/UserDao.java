package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.User;

//ユーザテーブル用のDao

public class UserDao {

	//ログインIDとパスワードに紐づくユーザ情報を返す

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// パスワードを暗号化
			PassEncryption psEcp = new PassEncryption();
			String ePass = psEcp.passEncryptiion(password);

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id=? and password=?";

			//SELECT文を実行、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, ePass);
			ResultSet rs = pStmt.executeQuery();

			/*主キーに紐づくレコードは1件のみなので、rs.next()は一回のみ実行*/
			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//すべてのユーザ情報を取得する

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE id > 1";

			//SELECT文を実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String UpdateDate = rs.getString("update_date");

				User user = new User(id, loginId, name, birthDate, password, createDate, UpdateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	// データベースにユーザを新規登録

	public void createUserInfo(String loginId, String password,
			String userName, String birthDate) {
		Connection conn = null;
		try {
			// データベースに接続
			conn = DBManager.getConnection();

			// パスワードを暗号化
			PassEncryption psEcp = new PassEncryption();
			String ePass = psEcp.passEncryptiion(password);

			// INSERT文を準備
			String sql = "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date)"
					+ "VALUES(?,?,?,?,now(),now())";

			// INSERT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, ePass);
			pStmt.setString(3, userName);
			pStmt.setString(4, birthDate);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// idに紐づくユーザ情報を返す

	public User findById(String id) {
		Connection conn = null;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id=?";

			// SELECT文を実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			/*主キーに紐づくレコードは1件のみなので、rs.next()は一回のみ実行*/

			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String UpdateDate = rs.getString("update_date");

			return new User(id1, loginId, name, birthDate, password, createDate, UpdateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/*
		ユーザ情報を更新
		@name
		@birthDate
		@updateDate
	*/

	public void updateUserInfo(String id, String password,
			String userName, String birthDate) {
		Connection conn = null;
		try {
			// データベースに接続
			conn = DBManager.getConnection();

			// パスワードを暗号化
			PassEncryption psEcp = new PassEncryption();
			String ePass = psEcp.passEncryptiion(password);

			// UPDATE文を準備
			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?,"
					+ "update_date = now() WHERE id = ?";

			// UPDATE文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, ePass);
			pStmt.setString(2, userName);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	// ユーザ情報を更新(パスワード変更なしver.)

	public void updateUserInfoNoP(String id,
			String userName, String birthDate) {
		Connection conn = null;
		try {
			// データベースに接続
			conn = DBManager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE user SET name = ?, birth_date = ?,"
					+ "update_date = now() WHERE id = ?";

			// UPDATE文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//データベースからユーザ情報を削除

	public void deleteUserInfo(String id) {
		Connection conn = null;
		try {
			// データベース接続
			conn = DBManager.getConnection();

			// DELETE文を準備
			String sql = "DELETE FROM user WHERE id = ?";

			// DELETE文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// ログインIDでユーザ情報を取得(新規登録チェック用)

	public User findByLoginId(String loginId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			// SELECT文を用意
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECT文を実行、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			/*login_idに紐づくレコードは1件のみなので、rs.next()は一回のみ実行*/
			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	// データベースから任意の入力情報で検索して結果を返す

	public List<User> seachAll(String loginId, String userName, String dateStart, String dateEnd) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE id > 1";

			if(!loginId.equals("")) {
				sql += " AND login_id = '" + loginId + "'";
			}
			if(!userName.equals("")) {
				sql += " AND name LIKE '%" + userName + "%'";
			}
			if(!dateStart.equals("")) {
				sql += " AND birth_date >= '" + dateStart + "'";
			}
			if(!dateEnd.equals("")) {
				sql += " AND birth_date <= '" + dateEnd + "'";
			}


			//SELECT文を実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String UpdateDate = rs.getString("update_date");

				User user = new User(id, loginId1, name, birthDate, password, createDate, UpdateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


}
