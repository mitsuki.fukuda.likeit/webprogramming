package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User userInfo = (User)session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		//idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);

		//ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("user", user);
		RequestDispatcher dispatchar = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatchar.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		boolean isError = false;

		// 未入力項目がある場合
		if(id.equals("") || userName.equals("") ||birthDate.equals("")) {
			isError = true;
		}

		// パスワードと確認用が一致しない場合
		if(!password.equals(passwordConf)) {
			isError = true;
		}

		// リクエストスコープにエラーメッセージをセット
		if(isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");


			// 入力欄に初期値を再入力
			UserDao userDao = new UserDao();
			User user = userDao.findById(id);
			request.setAttribute("user", user);


			// クリエイトjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}


		if(password.equals("") && passwordConf.equals("")) {

			UserDao userDao = new UserDao();
			userDao.updateUserInfoNoP(id, userName, birthDate);


		}else {
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDao userDao = new UserDao();
			userDao.updateUserInfo(id, password, userName, birthDate);

		}

		// ユーザ一覧画面にリダイレクト
		response.sendRedirect("UserListServlet");


	}

}
