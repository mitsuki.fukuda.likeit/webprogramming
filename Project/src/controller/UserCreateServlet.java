package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreate
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User userInfo = (User)session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		boolean isError = false;

		// 未入力項目がある場合
		if(loginId.equals("") || password.equals("") || passwordConf.equals("")
				|| userName.equals("") ||birthDate.equals("")) {
			isError = true;
		}

		// パスワードと確認用が一致しない場合
		if(!password.equals(passwordConf)) {
			isError = true;
		}

		// 既に登録されたログインIDが入力された場合
		UserDao userDao = new UserDao();

		if(userDao.findByLoginId(loginId) != null) {
			isError = true;
		}

		// リクエストスコープにエラーメッセージをセット
		if(isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// クリエイトjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.createUserInfo(loginId, password, userName, birthDate);

		//ユーザ一覧にリダイレクト
		response.sendRedirect("UserListServlet");
	}

}
