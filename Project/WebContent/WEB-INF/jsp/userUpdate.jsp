<%@ page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ユーザー情報更新</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
			<span class ="navbar-text display-name">
				${userInfo.name}さん
			</span>
			<a class="nav-link logout-link" href="LogoutServlet">ログアウト</a>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<h1 align="center">ユーザー新規更新</h1>

	<!-- 新規登録入力ボックス -->
	<div class="container">

		<!-- エラーメッセージ()エラーがある場合のみ表示 -->
		<c:if test="${errMsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>

		<div class="update-form">
			<form action="UserUpdateServlet" method="post" class="form-horizontal">

				<input type="hidden" name="id" value="${user.id}">

				<div class="form-group row">
					<label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
					<div class="col-sm-8">
						<p class="form-control-plaintext">${user.loginId}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="password" class="col-sm-4 col-form-label">パスワード</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group row">
					<label for="passwordConf" class="col-sm-4 col-form-label">パスワード(確認)</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="passwordConf">
					</div>
				</div>

				<div class="form-group row">
					<label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="userName" value="${user.name}">
					</div>
				</div>

				<div class="form-group row">
					<label for="birthDate" class="col-sm-4 col-form-label">生年月日</label>
					<div class="col-sm-4">
						<input type="date" class="form-control" name="birthDate" value="${user.birthDate}">
					</div>
				</div>

				<div class="submit-button">
					<button type="submit" class="btn btn-primary btn-lg btn-block">更新</button>
				</div>
			</form>
		</div>

		<div class="return-button">
			<a class="btn btn-primary" href="UserListServlet">戻る</a>
		</div>
	</div>

</body>
</html>