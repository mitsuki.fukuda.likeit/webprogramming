<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>ログイン画面</title>
	<!-- BootstrapのCSS読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<!-- オリジナルCSS読み込み -->
	<link rel="stylesheet" href="css/index.css">

</head>
<body>

	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
		</nav>
	</header>
	<!-- /heder -->

	<!-- body -->
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<form class="form-signin" action="LoginServlet" method="post">
		<div class="rogin-tytle">
			<h3 class="h3 mb-3 font-weight-normal">ログイン画面</h3>
		</div>

		<div class="form-label-group">
			<label for="inputLoginId">ログインID</label>
			<input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインIDを入力してください" autofocus>
		</div>

		<div class="form-label-group">
			<label for="inputPassword">Password</label>
	        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
        </div>

      <button type="submit" class="btn btn-primary btn-block btn-lg">ログイン</button>
	</form>
</body>
</html>