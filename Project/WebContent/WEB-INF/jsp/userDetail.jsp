<%@ page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ユーザ商法詳細参照画面</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
			<span class ="navbar-text display-name">
				${userInfo.name}さん
			</span>
			<a class="nav-link logout-link" href="LogoutServlet">ログアウト</a>
		</nav>
	</header>
	<!-- /header -->

	<h1 align="center">ユーザ情報参照</h1>

	<!-- 新規登録入力ボックス -->
	<div class="container">

		<!-- エラーメッセージ()エラーがある場合のみ表示 -->
		<c:if test="${errMsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>
		<!-- /エラーメッセージ -->

		<div class="detail-form">
			<form action="UserDetailServlet" method="post" class="form-horizontal">
				<div class="form-group row">
					<label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
					<div class="col-sm-8">
						  <input type="text" readonly class="form-control-plaintext" value="${user.loginId}">
					</div>
				</div>

				<div class="form-group row">
					<label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
					<div class="col-sm-8">
						<p class="form-control-planintext">${user.name}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="birthDate" class="col-sm-4 col-form-label">生年月日</label>
					<div class="col-sm-4">
						<p class="form-control-planintext">${user.birthDate}</p>
					</div>
				</div>

				<div class="form-group row">
					<label for="createDate" class="col-sm-4 col-form-label">新規登録日時</label>
					<div class="col-sm-4">
						<p class="form-control-planintext">${user.createDate}</p>
					</div>
				</div>

					<div class="form-group row">
					<label for="updateDate" class="col-sm-4 col-form-label">更新日時</label>
					<div class="col-sm-4">
						<p class="form-control-planintext">${user.updateDate}</p>
					</div>
				</div>

			</form>
		</div>

		<div class="return-button">
			<a class="btn btn-primary" href="UserListServlet">戻る</a>
		</div>
	</div>

</body>
</html>