<%@ page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<title>ユーザー一覧</title>
</head>
<body>


	<!-- header -->
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
			<span class ="navbar-text display-name">
				${userInfo.name}さん
			</span>
			<a class="nav-link logout-link" href="LogoutServlet">ログアウト</a>
		</nav>
	</header>

	<!-- /header -->

	<!-- body -->



	<div class="container">

		<h1 align="center">ユーザ一覧</h1><br>

		<!-- 新規登録ボタン 管理者のみ実行可能-->
		<c:if test="${userInfo.loginId =='admin'}">
			<div class="create-button">
				<a class="btn btn-primary" href="UserCreateServlet">新規登録</a>
			</div>
		</c:if>

		<!-- 検索ボックス -->
		<!-- TODO 未実装：検索機能 -->
		<div class="search-form">
			<div class="panel-body">
				<form action="UserListServlet" method="post" class="form-horizontal">
					<div class="form-group row">
						<label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="loginId">
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="userName">
						</div>
					</div>

					<div class="form-group row">

						<label for="birthDate" class="col-sm-4 col-form-label">生年月日</label>

						<div class="row col-sm-7">
							<div class="col-sm-4">
								<input type="date" class="form-control" name="date-start">
							</div>

							<div class="col-sm-1 text-center">~</div>

							<div class="col-sm-4">
								<input type="date" class="form-control" name="date-end">
							</div>
						</div>
					</div>
					<div class="text-right">
						<button type="submit" class="btn btn-primary">検索</button>
					</div>
				</form>
			</div>
		</div>
		<!-- /検索ボックス -->

		<!-- 検索結果一覧 -->
		<div class="table-responsive">
			<table class="table table-hover">
				<thead class="thead-light"><!-- ヘッダーにはdarkかlightしか使えない -->
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<!-- TODO 未実装：ログインボタンの表示制御をおこなう -->
							<td>
									<a href="UserDetailServlet?id=${user.id}" class="btn btn-primary">詳細</a>
								<c:if test="${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}">
									<a href="UserUpdateServlet?id=${user.id}" class="btn btn-success">更新</a>
									<a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger">削除</a>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>