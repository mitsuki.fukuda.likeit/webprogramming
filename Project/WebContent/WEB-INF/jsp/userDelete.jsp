<%@ page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ユーザ削除確認画面</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
	<!-- header -->
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
			<span class ="navbar-text display-name">
				${userInfo.name}さん
			</span>
			<a class="nav-link logout-link" href="LogoutServlet">ログアウト</a>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">
		<form action="UserDeleteServlet" method="post">

			<input type="hidden" name="id" value="${user.id}">

			<p>ユーザーID ${user.id} を削除しますか？</p>

			<div class="col-xs-4">
				<a class="btn btn-light" href="UserListServlet">いいえ、削除しません</a>

				<!-- テーブルからゆーざ削除して遷移 -->
				<button type="submit" class="btn btn-outline-danger">削除する</button>
			</div>
		</form>
	</div>
</body>
</html>