create database usermanagement default character 
  set
    utf8; 

drop table user; 

use usermanagement; 

create table user ( 
  id serial primary key auto_increment, 
  login_id varchar (255) unique not null, 
  name varchar (255) unique not null, 
  birth_date date not null, 
  password varchar (255) not null, 
  create_date datetime not null, 
  update_date datetime not null
); 

insert 
  into user ( 
    id, 
    login_id, 
    name, 
    birth_date, 
    password, 
    create_date, 
    update_date
  ) 
  values ( 
    1, 
    'admin', 
    '�Ǘ���', 
    '1995/08/30', 
    'password', 
    '2019/04/20 15:02:46', 
    '2019/04/25 17:11:38'
  );

